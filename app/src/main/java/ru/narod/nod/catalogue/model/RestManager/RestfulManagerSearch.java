package ru.narod.nod.catalogue.model.RestManager;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import ru.narod.nod.catalogue.model.Model;

public class RestfulManagerSearch extends RestfulManager {

    void startRestAPI(String url, Callback apiRequestListener) {
        Retrofit retrofit = super.prepareRestAPI();

        Call<ResponseBody> call = null;

        if (url != null) {
            //separator to divide 1.Search criteria 2.Offset(next 50 items)
            String[] criteriaAndOffset = url.split(Model.getInstance().getSEPARATOR());

            Map<String, String> map = new HashMap<>();
            map.put("q", criteriaAndOffset[0].trim());
            map.put("offset", criteriaAndOffset[1].trim());
            call = retrofit.create(MercadolibreClientMain.class)
                    .mercadoSearch(map);
        }

        if (call != null) {
            call.enqueue(apiRequestListener);
        }
    }

    @Override
    protected Object doInBackground(Object[] params) {
        String url = (String) params[0];
        Callback apiRequestListener = (Callback) params[1];

        startRestAPI(url, apiRequestListener);

        return null;
    }
}
