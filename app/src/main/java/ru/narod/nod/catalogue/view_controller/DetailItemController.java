package ru.narod.nod.catalogue.view_controller;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.narod.nod.catalogue.R;
import ru.narod.nod.catalogue.model.Item;
import ru.narod.nod.catalogue.model.Model;
import ru.narod.nod.catalogue.model.RestManager.JSonParser;
import ru.narod.nod.catalogue.model.RestManager.RestfulManager;
import ru.narod.nod.catalogue.model.RestManager.RestfulManagerDescription;
import ru.narod.nod.catalogue.model.RestManager.RestfulManagerItemsAndImages;
import ru.narod.nod.catalogue.model.SwipeDetector;
import ru.narod.nod.catalogue.model.dataBase.DBHelperVisitedItems;
import ru.narod.nod.catalogue.model.dataBase.DataKeepContract;

/**
 * Product number, title, description, price and image
 */

public class DetailItemController extends AppCompatActivity {

    //region Variable declaration
    private final static int MAXIMUM_LAST_VISITED_NUMBER = 5;
    private ImageView emptyImage;
    private Item item;
    private final String TAG = "myLogs." + getClass().getSimpleName();
    private ArrayList<ImageView> images;
    private int currentPicture = 0;
    private String parsedNumber = "";
    private SoundPool soundPool;

    @BindView(R.id.detail_tvNumber)
    TextView detail_tvNumber;
    @BindView(R.id.detail_tvTitle)
    TextView detail_tvTitle;
    @BindView(R.id.detail_tvDescription)
    TextView detail_tvDescription;
    @BindView(R.id.simple_progress_bar)
    com.wang.avi.AVLoadingIndicatorView simple_progress_bar;
    @BindView(R.id.detail_tempImageWhiteBlank)
    ImageView detail_tempImageWhiteBlank;
    @BindView(R.id.detail_ll_images)
    LinearLayout detail_ll_images;
    @BindView(R.id.detail_tempImageNoPhoto)
    ImageView detail_tempImageNoPhoto;


    //Gestures
    private GestureDetector gestureDetector;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    //endregion

    //region Callbacks
    //Getting a description of an item
    Callback apiRequestListenerDescription = new Callback<ResponseBody>() {
        @Override
        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
            try {
                Log.i(TAG, " DetailItemController: apiRequestListenerImages is started");

                String resp = null;
                try {
                    if (response.body() != null)
                        resp = response.body().string();
                    else
                        resp = "";
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.i(TAG, " DetailItemController: apiRequestListenerDescription is started");
                if (resp != null) {
                    JSonParser jSonParser = new JSonParser();
                    String parsedDescription = jSonParser.parseDescription(resp, getApplicationContext());
                    detail_tvDescription.setText(parsedDescription);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            try {
                Log.i(TAG, " the apiRequestListenerDescription response is onFailure() method, error is: " + t.toString());
//            Toast.makeText(getApplicationContext(),
//                    "A connection cannot be established. Try again later...", Toast.LENGTH_SHORT).show();
                detail_tvDescription.setText(R.string.no_description);
//            onBackPressed();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    //Getting images of an item
    Callback apiRequestListenerImages = new Callback<ResponseBody>() {
        @Override
        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
            try {
                Log.i(TAG, " DetailItemController: apiRequestListenerImages is started");

                String resp = null;
                try {
                    if (response.body() != null)
                        resp = response.body().string();
                    else
                        resp = "";
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (resp != null) {
                    detail_tempImageNoPhoto.setBackground(null);
                    detail_ll_images.removeAllViews(); //delete the loading spinner pic from layout

                    int widthOfScreen = detail_ll_images.getMeasuredWidth();
                    JSonParser jSonParser = new JSonParser();
                    images = jSonParser.parseImages(resp, item, getApplicationContext(), widthOfScreen);
                    //setting the first image if there is one or more pictures in the array
                    if (detail_ll_images.getChildCount() == 0 && images.size() > 0) {
                        detail_ll_images.addView(images.get(0));
                        Log.i(TAG, " DetailItemController: apiRequestListenerImages, image was added to view: " + images.get(0));
                        //                        detail_ll_images.getChildAt(0).setScaleY((float) 1.4);
                        //                        detail_ll_images.getChildAt(0).setScaleX((float) 1.4);
                    } else { //else setting the "no_image" pic
                        ImageView imageView = new ImageView(getApplicationContext());
                        imageView.setImageResource(R.mipmap.no_image);
                        detail_ll_images.addView(imageView);
                    }
                    simple_progress_bar.setVisibility(View.INVISIBLE);
                    detail_tempImageWhiteBlank.setBackground(null);

                    detail_ll_images.setClickable(true);


                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            saveDetailedViewToDB();
                        }
                    };
                    new Thread(runnable).start();


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            try {
                Log.i(TAG, " the apiRequestListenerImages response is onFailure() method, error is: " + t.toString());
                //            Toast.makeText(getApplicationContext(),
                //                    "A connection cannot be established. Try again later...", Toast.LENGTH_SHORT).show();
                simple_progress_bar.setVisibility(View.INVISIBLE);
                //            onBackPressed();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    //endregion

    @Override
    protected void onDestroy() {
        super.onDestroy();
        apiRequestListenerDescription = null;
        apiRequestListenerImages = null;
        if (soundPool != null) soundPool.release();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_item_view);

        Intent intent = getIntent();
        int position = intent.getIntExtra("ItemPosition", 0);
        String whoSentIntent = intent.getStringExtra("WhoSentIntent");
        switch (whoSentIntent) {
            case "SearchResultController":
                item = Model.getInstance().getSearchResultList().get(position);
                break;
            case "LastVisitedItemsController":
                item = Model.getInstance().getLastVisitedList().get(position);
                break;

        }

        //Using the Butter Knife tech to simplify code
        ButterKnife.bind(this);

        emptyImage = new ImageView(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            emptyImage.setImageDrawable(getDrawable(R.mipmap.no_image));
        }

        images = new ArrayList<>();

        gestureDetector = initGestureDetector();
        detail_ll_images.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                //changePicture();
                return gestureDetector.onTouchEvent(event);
            }
        });

        //Getting a number (id) of an item
        parsedNumber = item.getId();
        detail_tvNumber.setText(parsedNumber);

        //Getting a title of an item
        String parsedTitle = item.getTitle();
        detail_tvTitle.setText(parsedTitle);

        //Getting a price of an item
        double parsedPrice = item.getPrice();
//        detail_tvPrice.setText(String.valueOf(parsedPrice));
        this.setTitle("Price: " + String.valueOf(parsedPrice));

        RestfulManager restfulMeliManager = new RestfulManagerDescription();
        //Getting description
        restfulMeliManager.execute(item.getDescriptionsUrl(), apiRequestListenerDescription);

        RestfulManager restfulMeliManagerImage = new RestfulManagerItemsAndImages();
        //Getting image
        restfulMeliManagerImage.execute(item.getId(), apiRequestListenerImages);
    }

    //Writing id of visited detail view item to a DB
    private void saveDetailedViewToDB() {
        DBHelperVisitedItems dbHelperLastVisited = new DBHelperVisitedItems(this);

        //Creating an object for data
        ContentValues cv = new ContentValues();

        //Connecting to a db
        SQLiteDatabase db = dbHelperLastVisited.getWritableDatabase();

        ArrayList<String> arrayListOfNumbers = dbHelperLastVisited.getDB(db);

        //Erasing DB to create a new one
        try {
            dbHelperLastVisited.clearDB(db);
        } catch (Exception e) {
            Log.d(TAG, "--- Cannot delete the table: ---");
        }

        Log.d(TAG, "--- Insert in a table: ---");

        //read the db to the Log
        //dbHelperLastVisited.readDB(db);

        int index = arrayListOfNumbers.indexOf(String.valueOf(parsedNumber));
        if (index >= 0)
            arrayListOfNumbers.remove(index);

        //Preparing an arrayList to write to DB
        arrayListOfNumbers.add(parsedNumber);
        while (arrayListOfNumbers.size() > MAXIMUM_LAST_VISITED_NUMBER) {
            arrayListOfNumbers.remove(0);
        }

        //cv.put(DataKeepContract.TableFields.COLUMN_NUMBER, parsedNumber);
        for (String el : arrayListOfNumbers) {
            cv.put(DataKeepContract.TableFields.COLUMN_NUMBER, el);
            //Inserting data to the table and getting its ID
            long rowID = db.insert(DataKeepContract.TableFields.TABLE_NAME, null, cv);
            Log.d(TAG, "row inserted, ID = " + rowID);
        }

        //read the db to the Log
//        dbHelperLastVisited.readDB(db);

        dbHelperLastVisited.close();
        Log.d(TAG, "dbHelperLastVisited is closed");
    }

    //Method for gestures when we want to swipe images of an item
    private GestureDetector initGestureDetector() {
        return new GestureDetector(new GestureDetector.SimpleOnGestureListener() {

            private SwipeDetector detector = new SwipeDetector();

            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                   float velocityY) {
                try {
                    if (detector.isSwipeDown(e1, e2, velocityY)) {
                    } else if (detector.isSwipeUp(e1, e2, velocityY)) {
                    } else if (detector.isSwipeLeft(e1, e2, velocityX)) {
                        changePicture("left");
                    } else if (detector.isSwipeRight(e1, e2, velocityX)) {
                        changePicture("right");
                    }
                } catch (Exception e) {
                    Log.e(TAG, " Something wrong in the initGestureDetector() method");
                }
                return false;
            }
        });
    }

    //Changing of detailed image when swiping the picture
    public void changePicture(String direction) {

        if (images.size() > 0) {
            if (direction.equals("right")) {
                //Animation to remove from left to right
                soundEffect();
                changePictureWithAnimation(0, 2000, false);

                if (currentPicture + 1 < images.size()) {
                    currentPicture++;
                    //Animation to appear from left to right
                    soundEffect();
                    changePictureWithAnimation(-2000, 0, true);
                } else {
                    currentPicture = 0;
                }

            } else if (direction.equals("left")) {
                //Animation to remove from right to left
                soundEffect();
                changePictureWithAnimation(0, -2000, false);

                if (currentPicture - 1 >= 0) {
                    currentPicture--;
                    //Animation to appear from right to left
                    soundEffect();
                    changePictureWithAnimation(2000, 0, true);
                } else {
                    currentPicture = images.size() - 1;
                }
            }

        } else {
            detail_ll_images.addView(emptyImage);
        }
    }

    //Sound effect when images change
    private void soundEffect() {
        if (soundPool == null)
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        soundPool.load(this, R.raw.beep001, 1);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                if (status == 0)
                    soundPool.play(sampleId, 0.01f, 0.01f, 1, 0, 1);
            }
        });
    }

    //Animation effect for images
    private void changePictureWithAnimation(float ax, float bx, boolean secondAnimation) {
//        AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
//        alphaAnimation.setDuration(300);
        if (secondAnimation)
            detail_ll_images.removeAllViews();
        TranslateAnimation translateAnimation = new TranslateAnimation(ax, bx, (float) 0, (float) 0);
        translateAnimation.setDuration(500);
        AnimationSet set = new AnimationSet(false); //10
        set.addAnimation(translateAnimation); //11
        if (secondAnimation)
            detail_ll_images.addView(images.get(currentPicture));
        detail_ll_images.getChildAt(0).setAnimation(set);
    }

    //region Menu making
    //Preparing the menu (action bar)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu() was started in SearchresultController");
        getMenuInflater().inflate(R.menu.menu_detail, menu);
//        menu_detailActivity = menu;

        return true;
    }

    //Treating of the menu (action bar) selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected() was started in SearchresultController");
        switch (item.getItemId()) {
            //Going back to the filter
            case R.id.menu_back:
                onBackPressed();
                break;
            //Going to the favorite
            case R.id.menu_favorites:
                startActivity(new Intent(this, LastVisitedItemsController.class));
                break;
            //Going to the main activity (home)
            case R.id.menu_home:
                Intent intent = new Intent(DetailItemController.this, MainView.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

}