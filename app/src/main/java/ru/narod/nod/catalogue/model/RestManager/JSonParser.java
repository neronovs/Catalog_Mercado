package ru.narod.nod.catalogue.model.RestManager;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import ru.narod.nod.catalogue.R;
import ru.narod.nod.catalogue.model.Item;
import ru.narod.nod.catalogue.model.Model;


public class JSonParser  {

    private final String TAG = "myLogs." + getClass().getSimpleName();
//    Model model = Model.getInstance();
//    String stringApiRequestListener = "";

    public JSonParser() {
        Log.i(TAG, " JSonParser constructor: is done!");
    }

    //Getting Items for LastVisitedItems controller
    public void lastVisitedItems(String resp) {
        Log.i(TAG, " JSonParser: lastVisitedItems() was started!");

        Item item = new Item();
        try {
            JSONObject jo = new JSONObject(resp);
            item.setId(jo.getString("id"));
            item.setTitle(jo.getString("title"));
            item.setDescriptionsUrl(jo.getString("id"));
            item.setPrice(jo.getDouble("price"));
            item.setThumbnailUrl(jo.getString("thumbnail"));
//            item.setImageUrl(jo.getString("id"));

            JSONArray tmpJA = null;
            try {
                tmpJA = jo.getJSONArray("pictures");
                Log.i(TAG, " DetailItemController: parseImages() images are: " + tmpJA);
                if (tmpJA.length() > 0) {
                    for (int i = 0; i < tmpJA.length(); i++) {
                        String concreteImageUrl = tmpJA.getJSONObject(i).get("url").toString();
                        item.setImagesUrl(concreteImageUrl);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            Model.getInstance().putLastVisitedItem(jo.getString("id"), item);
            Model.getInstance().getLastVisitedList().add(item);
            Log.i(TAG, " JSonParser: lastVisitedItems() was finished properly!");

        } catch (JSONException e) {
            e.printStackTrace();
            Log.i(TAG, " JSonParser: lastVisitedItems() some ERROR happened!");
        }
    }


    //Taking an Array of Items
    public Item[] getArrayOfItems(String jsonString) {
        JSONObject jsonObjectMain = null;

        try {
            jsonObjectMain = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray jsonArrayItems = null;
        try {
            assert jsonObjectMain != null;
            jsonArrayItems = jsonObjectMain.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        assert jsonArrayItems != null;
        Item[] resultOfParsing = new Item[jsonArrayItems.length()];

//        Map dict = new HashMap();
//        Iterator iter = jsonObjectMain.keys();
//        Log.d(TAG, "iter: " + iter);
//        int i = 0;
//        while(iter.hasNext()) {
//            String key = (String) iter.next();
//            Log.d(TAG, "key: " + key);
        for (int i = 0; i < jsonArrayItems.length(); i++) {
            try {
                JSONObject jo = jsonArrayItems.getJSONObject(i);
                resultOfParsing[i] = new Item();
                resultOfParsing[i].setId(jo.getString("id"));
                resultOfParsing[i].setTitle(jo.getString("title"));
                resultOfParsing[i].setDescriptionsUrl(jo.getString("id"));
                resultOfParsing[i].setPrice(jo.getDouble("price"));
                resultOfParsing[i].setThumbnailUrl(jo.getString("thumbnail"));
//                resultOfParsing[i].setImageUrl(jo.getString("id") /*+ "/pictures"*/);
//                JSONArray ja = jo.getJSONArray("pictures");
//                for (int j = 0; j < ja.length(); j++) {
//                    JSONObject joFromJa = ja.getJSONObject(j);
//                    resultOfParsing[i].setImagesUrl(joFromJa.getString("secure_url") /*+ "/pictures"*/);
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Collections.addAll(Model.getInstance().getSearchResultList(), resultOfParsing);

        return resultOfParsing;
    }

    //Taking a description from json
    public String parseDescription(String jsonString, Context context) {
        String parsedDescription = "";
        try {
            JSONArray tmpJA = new JSONArray(jsonString);
            JSONObject tmpJO = tmpJA.getJSONObject(0);
            parsedDescription = tmpJO.getString("plain_text");
            Log.i(TAG, " DetailItemController: parseDescription() description is: " + parsedDescription);
            if (parsedDescription.equals(""))
                parsedDescription = context.getResources().getString(R.string.no_description); //"No description";
        } catch (JSONException e) {
            Log.i(TAG, " DetailItemController: parseDescription() an Error in json: " + e);
            parsedDescription = context.getResources().getString(R.string.no_description); //"No description"
        }
        return parsedDescription;
    }

    //Taking image URLs from json, puts them to an arrayList and returns
    public ArrayList<ImageView> parseImages(String jsonString, Item item, Context context, int widthOfScreen) {
        ArrayList<ImageView> images = new ArrayList<>();
        try {
            JSONObject tmpJO = new JSONObject(jsonString);
            JSONArray tmpJA = tmpJO.getJSONArray("pictures");
            Log.i(TAG, " DetailItemController: parseImages() images are: " + tmpJA);
            if (tmpJA.length() > 0) {
                for (int i = 0; i < tmpJA.length(); i++) {
                    String concreteImageUrl = tmpJA.getJSONObject(i).get("url").toString();
                    item.setImagesUrl(concreteImageUrl);

                    Log.i(TAG, " +++ DetailItemController: parseImages() Glide is started");
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .override(widthOfScreen)
                            .error(R.mipmap.no_image)
                            .priority(Priority.HIGH);

                    ImageView imageView = new ImageView(context);
                    Glide.with(context)
                            .load(concreteImageUrl)
                            .apply(options)
                            .into(imageView)
                    ;

                    images.add(imageView);

                    Log.i(TAG, " DetailItemController: parseImages() is finished with tmpJA.length() > 0");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return images;
    }
}
