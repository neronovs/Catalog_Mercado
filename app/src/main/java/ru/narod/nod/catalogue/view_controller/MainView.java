package ru.narod.nod.catalogue.view_controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.Tracking;
import net.hockeyapp.android.UpdateManager;
import net.hockeyapp.android.metrics.MetricsManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.narod.nod.catalogue.R;
import ru.narod.nod.catalogue.model.Model;

public class MainView extends AppCompatActivity
        implements View.OnClickListener, IMainView {

    //region Variable declaration
    private final String TAG = "myLogs." + getClass().getSimpleName();
    @BindView(R.id.etSearchCriteria)
    EditText etSearchCriteria;
    @BindView(R.id.btnSearch)
    Button btnSearch;
    @BindView(R.id.btnLastVisited)
    Button btnLastVisited;
    @BindView(R.id.btnClearEtSearchCriteria)
    Button btnClearEtSearchCriteria;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view);

        //Setting the application cotext to the Model
        Model.getInstance().setAppContext(this);

        //Using the Butter Knife tech to simplify code
        ButterKnife.bind(this);

        Model.getInstance().setBase_url(getResources().getString(R.string.meli_redirect_uri));

        etSearchCriteria = (EditText) findViewById(R.id.etSearchCriteria);
        if (!Model.getInstance().getEtSearchCriteria().equals("")) {
            etSearchCriteria.setText(Model.getInstance().getEtSearchCriteria());
        }

        btnSearch.setOnClickListener(this);
        btnLastVisited.setOnClickListener(this);
        btnClearEtSearchCriteria.setOnClickListener(this);

        Model.getInstance().setSearchMainString(getResources().getString(R.string.search_main_string));

        //region HockeyApp inits
        checkForCrashes();
        checkForUpdates();
        MetricsManager.register(this, getApplication());
        //endregion
    }

    //region HockeyApp methods and hoockey's functionality are here
    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    @Override
    public void onPause() {
        Tracking.stopUsage(this);
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Model.getInstance().setAppContext(null);
        unregisterManagers();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Tracking.startUsage(this);
    }
    //endregion

    //Starting of the search process when clicked the "serach" btn of the same action bar item
    public void startSearching() {

        if (etSearchCriteria.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Enter item you want to find",
                    Toast.LENGTH_SHORT).show();
        } else {
            //Hiding a keyboard
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context
                        .INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                Log.i(TAG, " startSearching(), the getWindowToken() method produced an exception");
            }


            //Showing the progress bar during the start search process
            startProgressBar();

            //Making btns unclickable when the process has started
            makeAllButonsUnclickable();

            //Checking for the Internet
            if (Model.isOnline(getApplicationContext())) {
                Log.i(TAG, " startSearching() is started!");
                Model.getInstance().setEtSearchCriteria(etSearchCriteria.getText().toString());


                Intent intent = new Intent(MainView.this, SearchResultController.class);
                startActivity(intent);

                stopProgressBar();
            } else {
                Toast.makeText(getApplicationContext(), "Check your Internet connection",
                        Toast.LENGTH_SHORT).show();
                Log.i(TAG, " startSearching(), the Internet is lost!");
            }

            makeAllButonsClickable();
        }
    }

    public void startHistory() {

        makeAllButonsUnclickable();

        if (Model.isOnline(getApplicationContext())) {
            Log.i(TAG, " MainView: startHistory() was started");
            Intent intent = new Intent(MainView.this, LastVisitedItemsController.class);
            startActivity(intent);
        } else {
            Log.i(TAG, " startHistory(), the Internet is lost!");
        }

        makeAllButonsClickable();
    }

    //Setting all driving elements unclickable
    private void makeAllButonsUnclickable() {
        btnLastVisited.setClickable(false);
        btnSearch.setClickable(false);
        findViewById(R.id.menu_filter).setClickable(false);
        findViewById(R.id.menu_favorites).setClickable(false);
    }
    //Setting all driving elements clickable
    private void makeAllButonsClickable() {
        btnLastVisited.setClickable(true);
        btnSearch.setClickable(true);
        findViewById(R.id.menu_filter).setClickable(true);
        findViewById(R.id.menu_favorites).setClickable(true);
    }


    //Stop showing the progress bar when the start search process is stopped
    public void stopProgressBar() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
    }

    //Start showing the progress bar
    public void startProgressBar() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
    }

    //region Menu creating *************
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "onPrepareOptionsMenu() was started in MainActivity");

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu() was started in MainActivity");
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    //Treating of the menu (action bar) selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected() was started in MainActivity");
        switch (item.getItemId()) {
            case R.id.menu_filter:
                startSearching();
                break;
            case R.id.menu_favorites:
                startHistory();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearch:
                startSearching();
                break;
            case R.id.btnLastVisited:
                startHistory();
                break;
            case R.id.btnClearEtSearchCriteria:
                etSearchCriteria.setText("");
                break;
        }
    }
}
