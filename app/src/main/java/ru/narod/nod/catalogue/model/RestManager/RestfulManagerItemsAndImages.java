package ru.narod.nod.catalogue.model.RestManager;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class RestfulManagerItemsAndImages extends RestfulManager {

    void startRestAPI(String url, Callback apiRequestListener) {
        Retrofit retrofit = super.prepareRestAPI();

        Call<ResponseBody> call = null;

        if (url != null) {
            call = retrofit.create(MercadolibreClientItemsAndImages.class)
                    .mercadoSearch(url);
        }

        if (call != null) {
            call.enqueue(apiRequestListener);
        }
    }

    @Override
    protected Object doInBackground(Object[] params) {
        String url = (String) params[0];
        Callback apiRequestListener = (Callback) params[1];

        startRestAPI(url, apiRequestListener);

        return null;
    }
}
