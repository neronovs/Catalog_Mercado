package ru.narod.nod.catalogue.view_controller;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.narod.nod.catalogue.R;
import ru.narod.nod.catalogue.model.Model;
import ru.narod.nod.catalogue.model.RestManager.JSonParser;
import ru.narod.nod.catalogue.model.RestManager.RestfulManager;
import ru.narod.nod.catalogue.model.RestManager.RestfulManagerItemsAndImages;
import ru.narod.nod.catalogue.model.dataBase.DBHelperVisitedItems;
import ru.narod.nod.catalogue.model.dataBase.DataKeepContract;
import ru.narod.nod.catalogue.view_controller.recycler_view_pack.ItemClickSupport;
import ru.narod.nod.catalogue.view_controller.recycler_view_pack.RVAdapter;


public class LastVisitedItemsController extends AppCompatActivity {

    private final String TAG = "myLogs." + getClass().getSimpleName();
    private JSonParser jSonParser;
    @BindView(R.id.progress_bar_last_visited) com.wang.avi.AVLoadingIndicatorView progress_bar_last_visited;
//    @BindView(R.id.progress_bar_last_visited) ProgressBar progress_bar_last_visited;

    @BindView(R.id.rv) RecyclerView rv;
    private RVAdapter adapter;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Model.getInstance().getLastVisitedList().clear(); //Clearing the lastvisited list
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.last_visited_items_view);
        setContentView(R.layout.search_result_view);
        Log.i(TAG, " onCreate() is started");

        Model.getInstance().getLastVisitedList().clear(); //Clearing the lastvisited list

        //Using the Butter Knife tech to simplify code
        ButterKnife.bind(this);

        //Showing the progress bar during the start process
        progress_bar_last_visited.setVisibility(View.VISIBLE);

        //Setting title as "Last visited"
        setTitle("Last visited");

        jSonParser = new JSonParser();


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                getLastVisitedFromDB();
            }
        };
        new Thread(runnable).start();


        //region RecyclerView is the place where fragments will be placed
        rv.setHasFixedSize(true);
        //Initing LinearLayoutManager to manage items in the RecyclerView
        rv.setLayoutManager(new LinearLayoutManager(this));
        //Setting up the onclick listener
        ItemClickSupport.addTo(rv).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Intent intent = new Intent(v.getContext(), DetailItemController.class);
                        intent.putExtra("ItemPosition", position);
                        intent.putExtra("WhoSentIntent", "LastVisitedItemsController");
//                        intent.putExtra("CameFrom", position);
                        startActivity(intent);
                    }
                }
        );
        adapter = new RVAdapter(Model.getInstance().getLastVisitedList());
        rv.setAdapter(adapter);
        //endregion

        Log.i(TAG, " onCreate() is finished");
    }

    //region Loading of the favorites
    public void getLastVisitedFromDB() {

        //Initialisation of DBHelperFavoritePurchases to load and save favorites to a database
        DBHelperVisitedItems dbHelperLastVisitedItems = new DBHelperVisitedItems(this);

        Log.d(TAG, "--- Rows in favorites: ---");

        //Connecting to a db
        SQLiteDatabase db = dbHelperLastVisitedItems.getWritableDatabase();

        //Making request for all data from a table ang getting the Cursor
        Cursor c = db.query(DataKeepContract.TableFields.TABLE_NAME,
                null, null, null, null, null, null);

        //read the db to LOG
        dbHelperLastVisitedItems.readDB(db);

        //Putting a cursor position to the first row of the massive
        //If it is no rows then it's returning false
        if (c.moveToFirst()) {

            //Determine column numbers by name in massive
            //final int _ID = c.getColumnIndex(DataKeepContract.TableFields._ID);
            final int COLUMN_NUMBER = c.getColumnIndex(DataKeepContract.TableFields.COLUMN_NUMBER);

//            itemsUrls = new ArrayList<>();

            do {
                //Getting values by column numbers
                //Getting and Converting image's URLs to load pictures

                final String idItemNumber = c.getString(COLUMN_NUMBER);

                //Getting a description of an item
                Callback apiRequestListener_lastVisitedItemsController = new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call,
                                           @NonNull Response<ResponseBody> response) {

                        Log.i(TAG, " DetailItemController: apiRequestListenerDescription is started");

                        String resp = null;
                        try {
                            resp = response.body().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (resp != null)
                            jSonParser.lastVisitedItems(resp);

                        adapter.notifyDataSetChanged();

                        //Hiding the progress bar
                        progress_bar_last_visited.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        Log.i(TAG, " the response is onFailure() method, error is: " + t.toString());
                        Toast.makeText(getApplicationContext(),
                                "A connection cannot be established. Try again later...", Toast.LENGTH_SHORT).show();
                    }
                };


                String url =
                        //getResources().getString(R.string.items_main_string) + //a string from the EditText field with a criteria
                        idItemNumber;// +
                //"#json"; //last string symbols
                RestfulManager restfulMeliManager = new RestfulManagerItemsAndImages();
                restfulMeliManager.execute(url, apiRequestListener_lastVisitedItemsController);


                //Going to the next row
                //If it's the last row then getting false and finish a cycle
            } while (c.moveToNext());
        } else
            Log.d(TAG, "0 rows");
        c.close();
        dbHelperLastVisitedItems.close();
    }
    //endregion


    //region Menu making
    //Preparing the menu (action bar)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        return true;
    }

    //Treating of the menu (action bar) selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //Going back to the filter
            case R.id.menu_back:
                onBackPressed();
                break;
            //Going to the favorite
            case R.id.menu_favorites:
                startActivity(new Intent(this, LastVisitedItemsController.class));
                break;
            //Going to the main activity (home)
            case R.id.menu_home:
                Intent intent = new Intent(LastVisitedItemsController.this, MainView.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

}
