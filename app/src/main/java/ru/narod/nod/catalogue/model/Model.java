package ru.narod.nod.catalogue.model;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Model {

    private final String SEPARATOR = ",;,;,;";
    private String base_url = "";
    private static final Model ourInstance = new Model();
    private Map<String, Item> lastVisited;
    private List<Item> searchResultList;
    private List<Item> lastVisitedList;
    private String searchMainString;
//    private ApiRequestListener apiRequestListener;
    private String etSearchCriteria = "";
    private Context appContext;
//    private RecyclerView rv;

    public static Model getInstance() {
        return ourInstance;
    }

    private Model() {
        lastVisited = new HashMap<>();
        searchResultList = new ArrayList<>();
        lastVisitedList = new ArrayList<>();
    }

    //The SEPARATOR getter
    public String getSEPARATOR() {
        return SEPARATOR;
    }

    //The base url setter/getter
    public String getBase_url() {
        return base_url;
    }
    public void setBase_url(String base_url) {
        this.base_url = base_url;
    }

    //The lastVisited array getter/setter
    public Map getLastVisited() {
        return lastVisited;
    }
    public Item getLastVisitedItem(String idOfItem) {
        return lastVisited.get(idOfItem);
    }
    public void setLastVisited(Map<String, Item> lastVisited) {
        this.lastVisited = lastVisited;
    }
    public void putLastVisitedItem(String idOfItem, Item item) {
        this.lastVisited.put(idOfItem, item);
    }

    //The searchMainString string getter/setter
    public String getSearchMainString() {
        return searchMainString;
    }
    public void setSearchMainString(String searchMainString) {
        this.searchMainString = searchMainString;
    }

    //The etSearchCriteria editText getter/setter
    public String getEtSearchCriteria() {
        return etSearchCriteria;
    }
    public void setEtSearchCriteria(String etSearchCriteria) {
        this.etSearchCriteria = etSearchCriteria;
    }

    //Using of the application context
    public Context getAppContext() {
        return appContext;
    }
    public void setAppContext(Context appContext) {
        this.appContext = appContext;
    }


    //The apiRequestListener callback getter/setter
//    public ApiRequestListener getApiRequestListener() {
//        return apiRequestListener;
//    }
//    public void setApiRequestListener(ApiRequestListener apiRequestListener) {
//        this.apiRequestListener = apiRequestListener;
//    }

    //The method of checking the Internet connection
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean isInternet = netInfo != null && netInfo.isConnectedOrConnecting();

        if (!isInternet)
            Toast.makeText(context, "The Internet connection is lost", Toast.LENGTH_SHORT).show();

        return isInternet;
    }

    //SearchReault List getters/setters
    public Item getSearchResultListItem(int index) {
        return searchResultList.get(index);
    }
    public List<Item> getSearchResultList() {
        return searchResultList;
    }
    public void addSearchResultListItem(Item item) {
        this.searchResultList.add(item);
    }
    public void setSearchResultList(List<Item> searchResultList) {
        this.searchResultList = searchResultList;
    }
    public List<Item> getLastVisitedList() { return lastVisitedList; }

//    //RecyclerView getter/setter
//    public RecyclerView getRv() {
//        return rv;
//    }
//    public void setRv(RecyclerView rv) {
//        this.rv = rv;
//    }

}
