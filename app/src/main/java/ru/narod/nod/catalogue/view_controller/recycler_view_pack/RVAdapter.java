package ru.narod.nod.catalogue.view_controller.recycler_view_pack;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.narod.nod.catalogue.R;
import ru.narod.nod.catalogue.model.Item;
import ru.narod.nod.catalogue.model.Model;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ItemViewHolder> {

    private List<Item> items;
    private final String TAG = "myLogs." + getClass().getSimpleName();

    // Define listener member variable
    private OnItemClickListener listener;

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public RVAdapter(List<Item> items) {
        this.items = items;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
//        CardView cv;
        Item item;
        TextView simple_tvTitle;
        TextView simple_tvPrice;
        ImageView simple_imageThumbnail;
        ProgressBar progressBar;
        private final String TAG = "myLogs." + getClass().getSimpleName();

        ItemViewHolder(final View itemView) {
            super(itemView);
//            cv = (CardView) itemView.findViewById(R.id.cv);
            simple_tvTitle = (TextView) itemView.findViewById(R.id.simple_tvTitle);
            simple_tvPrice = (TextView) itemView.findViewById(R.id.simple_tvPrice);
            simple_imageThumbnail = (ImageView) itemView.findViewById(R.id.simple_imageThumbnail);
            progressBar = (ProgressBar) itemView.findViewById(R.id.simple_progress_bar);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(v, position);
                            Log.i("", "!!!Click");
//                            listener.onItemClick(simple_imageThumbnail, position);
//                            listener.onItemClick(simple_tvPrice, position);
//                            listener.onItemClick(simple_tvTitle, position);
                        }
                    }
                }
            });
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.simple_item_view, viewGroup, false);
        ItemViewHolder ivh = new ItemViewHolder(v);
        return ivh;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int position) {
        itemViewHolder.item = items.get(position);
        itemViewHolder.simple_tvTitle.setText(items.get(position).getTitle());
        itemViewHolder.simple_tvPrice.setText(String.valueOf(items.get(position).getPrice()));

        itemViewHolder.progressBar.setVisibility(View.INVISIBLE);


        if (!items.get(position).getThumbnailUrl().trim().equals("")) {
            Picasso.with(Model.getInstance().getAppContext())
                    .load(items.get(position).getThumbnailUrl().trim())
                    .into(itemViewHolder.simple_imageThumbnail);
        } else {
            Log.i("Url 4 Picasso is empty!", items.get(position).getThumbnailUrl().trim());
        }

//        itemViewHolder.cv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i("", "!!!Click");
////                List<Item> arr = Model.getInstance().getSearchResultList();
////                arr.add(position, arr.get(position));
////                notifyItemInserted(position);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}