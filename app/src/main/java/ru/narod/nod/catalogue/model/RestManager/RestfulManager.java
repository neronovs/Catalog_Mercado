package ru.narod.nod.catalogue.model.RestManager;

import android.os.AsyncTask;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import ru.narod.nod.catalogue.model.Model;


public abstract class RestfulManager extends AsyncTask {
    protected final String TAG = "myLogs." + getClass().getSimpleName();
//    Gson gson;

    public Retrofit prepareRestAPI() {
//        gson = new GsonBuilder().create();
        String url_base = Model.getInstance().getBase_url();
        String url_criteria = Model.getInstance().getEtSearchCriteria();

        //Change timeout to 15 secs
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
//                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .baseUrl(url_base)
                .build();

        return retrofit;
    }

    abstract void startRestAPI(String url, Callback apiRequestListener);
}

interface MercadolibreClientMain {
    @GET("/sites/MLU/search")
    Call<ResponseBody> mercadoSearch(@QueryMap Map<String, String> filters);
}

interface MercadolibreClientItemsAndImages {
    @GET("/items/{id}/")
    Call<ResponseBody> mercadoSearch(@Path("id") String id);
}

interface MercadolibreClientDescription {
    @GET("/items/{id}/descriptions/")
    Call<ResponseBody> mercadoSearch(@Path("id") String id);
}