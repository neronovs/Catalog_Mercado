package ru.narod.nod.catalogue.view_controller;

public interface IMainView {
    void startProgressBar();

    void stopProgressBar();

    void startSearching();

    void startHistory();
}
