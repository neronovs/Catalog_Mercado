package ru.narod.nod.catalogue.view_controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.narod.nod.catalogue.R;
import ru.narod.nod.catalogue.model.Item;
import ru.narod.nod.catalogue.model.Model;
import ru.narod.nod.catalogue.model.RestManager.JSonParser;
import ru.narod.nod.catalogue.model.RestManager.RestfulManager;
import ru.narod.nod.catalogue.model.RestManager.RestfulManagerSearch;
import ru.narod.nod.catalogue.view_controller.recycler_view_pack.ItemClickSupport;
import ru.narod.nod.catalogue.view_controller.recycler_view_pack.RVAdapter;

public class SearchResultController extends AppCompatActivity {

    private final String TAG = "myLogs." + getClass().getSimpleName();
    private boolean updateList = true;
    private int offset;
    @BindView(R.id.progress_bar_last_visited) com.wang.avi.AVLoadingIndicatorView progress_bar_last_visited;
    @BindView(R.id.simple_no_result) TextView simple_no_result;

    RecyclerView rv;
    RVAdapter adapter;
    LinearLayoutManager llm;
    //region the RecyclerView.OnScrollListener listener
    RecyclerView.OnScrollListener listener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            double scrollRange = recyclerView.computeVerticalScrollRange();
            double scrollPosition = recyclerView.computeVerticalScrollOffset();
            if (scrollPosition / scrollRange >= 0.4 && updateList) {
                Log.i(TAG, " OnScrollChangeListener() percent of scrolled items is " + scrollPosition / scrollRange);

                updateList = false;

                String urlPlus =
//                        Model.getInstance().getSearchMainString() + //addition for search url
                        Model.getInstance().getEtSearchCriteria() + //a string from the EditText field with a criteria
                                Model.getInstance().getSEPARATOR() + //separator to divide 1.Search criteria 2.Offset(next 50 items)
                                offset;
//                        "#json"; //last string symbols
                RestfulManager rfm = new RestfulManagerSearch();

                rfm.execute(urlPlus, apiRequestListener_searchResultController);
            }
        }
    };
    //endregion

    //A callback for addition filling of the result activity with another 50 items to the BOTTOM
    Callback<ResponseBody> apiRequestListener_searchResultController = new Callback<ResponseBody>() {
        @Override
        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
            Log.i(TAG, " onCreate(): ApiRequestListener: gotten an answer from +50_items BOTTOM");

            String resp = null;
            try {
                resp = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSonParser jSonParser = new JSonParser();
            jSonParser.getArrayOfItems(resp);
            fill();
        }

        @Override
        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            Log.i(TAG, " the response is onFailure() method, error is: " + t.toString());
            Toast.makeText(getApplicationContext(),
                    "A connection cannot be established. Try again later...", Toast.LENGTH_SHORT).show();
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        listener = null;
        apiRequestListener_searchResultController = null;
        Model.getInstance().getSearchResultList().clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result_view);
        Log.i(TAG, " onCreate() is started");

        //Using the Butter Knife tech to simplify code
        ButterKnife.bind(this);

        //Showing the progress bar during the start process
        progress_bar_last_visited.setVisibility(View.VISIBLE);

        //Offset init
        offset = 0;

        //Setting title of the view
        setTitle("Search result");

        //RecyclerView is the place where fragments will be placed
        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        //Initing LinearLayoutManager to manage items in the RecyclerView
        llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        //Set ScrollListener to rv
        rv.addOnScrollListener(listener);

        //Setting up the oncliick listener
        ItemClickSupport.addTo(rv).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Intent intent = new Intent(v.getContext(), DetailItemController.class);
                        intent.putExtra("ItemPosition", position);
                        intent.putExtra("WhoSentIntent", "SearchResultController");
                        startActivity(intent);
                    }
                }
        );

        //Making the first restful request
        String url =
                Model.getInstance().getEtSearchCriteria() + //a string from the EditText field with a criteria
                Model.getInstance().getSEPARATOR() + //separator to divide 1.Search criteria 2.Offset(next 50 items)
                "0";
        RestfulManager restfulMeliManager = new RestfulManagerSearch();
        restfulMeliManager.execute(url, apiRequestListener_searchResultController);

        Log.i(TAG, " onCreate() is finished");
    }

    //Filling the result activity with simple views
    private void fill() {
        List<Item> itemArray = Model.getInstance().getSearchResultList();

        if (itemArray.size() > 0) {

            updateList = true;

            if (adapter == null) {
                adapter = new RVAdapter(itemArray);
                rv.setAdapter(adapter);
            }

            adapter.notifyDataSetChanged();
            offset += 50;

            //Hiding the progress bar
            progress_bar_last_visited.setVisibility(View.INVISIBLE);

            Log.i(TAG, " fill(): getting another 50 items to the search_result_view at BOTTOM");

        } else {
            //Hiding the progress bar
            progress_bar_last_visited.setVisibility(View.INVISIBLE);
            simple_no_result.setVisibility(View.VISIBLE);
        }

        Log.d(TAG, " SearchresultController: fill() was finished");
    }


    //region Menu making
    //Preparing the menu (action bar)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu() was started in SearchresultController");
        getMenuInflater().inflate(R.menu.menu_result, menu);

        return true;
    }

    //Treating of the menu (action bar) selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected() was started in SearchresultController");
        switch (item.getItemId()) {
            //Going back to the filter
            case R.id.menu_back:
                onBackPressed();
                break;
            //Going to the favorite
            case R.id.menu_favorites:
                startActivity(new Intent(this, LastVisitedItemsController.class));
                break;
            //Going to the main activity (home)
            case R.id.menu_home:
                Intent intent = new Intent(SearchResultController.this, MainView.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

}
